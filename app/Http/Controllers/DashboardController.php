<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class DashboardController extends Controller
{


    public function index(){

        $posts = Post::with(['user','comment'])->get();
//        return $posts;die();

        Log::info($posts);
        return view('dashboard.index',compact('posts'));
    }
}
