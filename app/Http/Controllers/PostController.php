<?php

namespace App\Http\Controllers;

use App\Post;
use App\Http\Requests\PostStoreRequest;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::with('user')->where('user_id', Auth::user()->id)->get();

        return view('dashboard.post.index',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostStoreRequest $request)
    {
        Log::info($request);
       $user = Post::create([
           'user_id' => Auth::user()->id,
           'post_title' => $request->input('post_title'),
           'post_body' => $request->input('post_body'),
           'post_file' => (!is_null($request->file('post_file'))) ? $request->file('post_file')->getClientOriginalName(): '',
           'status' => 'active',
           'published_at' => date('Y-m-d H:i:s')
       ]);

        if(!is_null($request->post_file)){
            $request->post_file->move(public_path('images'), $request->file('post_file')->getClientOriginalName());
        }

//       $this->handleUploadImage($request->file('post_file'));

        return redirect()->back();


    }

    public function handleUploadImage($post_file){
        if(!is_null($post_file)){
            $post_file->move(public_path('/images').'temp');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $post = Post::findOrFail($id);

       return  view('dashboard.post.edit',compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(PostStoreRequest $request, $id)
    {

        $post = Post::find($id);

        $post->post_title = $request->post_title;
        $post->post_body = $request->post_body;
        $post->post_file = $request->post_file;

        $post->save();

       return redirect()->route('post.index');




    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post = Post::findOrFail($post);


    }



}
