<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';
    protected $fillable = ['user_id','post_id','comment_msg'];

    public function user(){
        return $this->belongsToMany(User::class);
    }

    public function post(){
        return $this->belongsTo(Post::class);
    }


}
