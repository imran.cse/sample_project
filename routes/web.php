<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => 'auth'],function (){
    Route::get('/', function () {
        return view('dashboard.index');
    });

    Route::get('/dashboard','DashboardController@index')->name('dashboard');
    Route::resource('post','PostController');
    Route::resource('message','MessageController');
    Route::resource('comment','CommentController');


});

Auth::routes();

Route::get('/cl',function (){
    Artisan::call('view:clear');
    Artisan::call('route:clear');
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    return 'cache cleared';
});
