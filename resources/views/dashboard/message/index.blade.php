@extends('layouts.backend')

@section('content')

   <div class="row">

           @if(isset($users) && count($users) > 0)
               @foreach($users as $user)
               <div class="col-3">

                   <object data="{{asset('images/users/'.$user->picture)}}" type="image/png">
                       <img src="{{asset('images/avatar/person.png')}}" alt="no image" width="50px">
                   </object>


                   <form method="post" action="{{route('message.store')}}">
                       @csrf
                       <input type="hidden" name="to_user" value="{{$user->id}}">
                       <label class="d-block" for="">Name : {{$user->name}}</label>
                       <textarea name="message_body" id="message_body" cols="20" rows="2" placeholder="write here"></textarea>
                       <button type="submit">Quick Msg</button>
                       <a class="btn btn-secondary" href="{{route('message.show',$user->id)}}">Msgs</a>
                   </form>
               </div>

               @endforeach

               @else
               <h4>No Users</h4>

            @endif



   </div>

@endsection
