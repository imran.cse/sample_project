@extends('layouts.backend')

@section('content')
    <h2>Messages</h2>
{{--    <label for=""> Recipent : {{$messages['to_user')}}</label>--}}


    <table class="table">
        <tr>
            <td>Date</td>
            <td>Recipent</td>
            <td>Message</td>
            <td>Actions</td>
        </tr>


        @if(isset($messages) && count($messages) > 0 )
            @foreach($messages as $m)
                <tr>
                    <td>{{$m->created_at}}</td>
                    <td>{{$m->user->name}}</td>
                    <td>{{$m->message_body}}</td>
                    <td>
                        <button>Del</button>
                    </td>
                </tr>
            @endforeach
        @endif


    </table>



@endsection
