@extends('layouts.backend')

@section('content')

    <div class="row">
        <div class="col-12">
            <h4>List of : Posts</h4>
            <a href="{{route('post.create')}}" class="btn btn-info float-right"> Add New</a>
        </div>
        <div class="col-12">
            <table class="table-striped table">

                    <tr>
                        <th>Post Title</th>
                        <th>Content</th>
                        <th>Attachment</th>
                        <th>Status</th>
                        <th>Author</th>
                        <th>Created</th>
                        <th>Actions</th>
                    </tr>



                @if(isset($posts) && count($posts)>0)
                    @foreach($posts as $post)
                    <tr>
                        <td>{{$post->post_title}}</td>
                        <td> {{Str::limit($post->post_body,200)}}</td>
                        <td>{{$post->post_file}}</td>
                        <td>{{$post->status}}</td>
                        <td>{{$post->user->name}}</td>
                        <td>{{$post->created_at}}</td>
                        <td>
                            <a href="{{route('post.edit',$post->id)}}" class="link">Edit</a>
                            <a href="" class="link">Del</a>
                        </td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="7"> No record found.</td>
                    </tr>

                @endif

            </table>
        </div>
    </div>




@endsection
