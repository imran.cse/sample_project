@extends('layouts.backend')

@section('content')



    <h3>Create New</h3>

    <form method="POST" action="{{route('post.update',$post->id)}}" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        <div class="form-group row">
            <label for="">post title </label>
            <input type="text" name="post_title" value="{{$post->post_title}}">
        </div>
        <div class="form-group row">
            <label for="">conent</label>
            <textarea rows="10" type="text" name="post_body">
                {{$post->post_body}}
            </textarea>
        </div>
        <div class="form-group row">
            <label for="">Attachment</label>


            <input type="file" name="post_file">
        </div>


        <div>
            <button type="submit">Submit</button>
        </div>

    </form>



@endsection
