@extends('layouts.backend')

@section('content')

    <h3>Create New</h3>

    <form method="post" action="{{route('post.store')}}" enctype="multipart/form-data">
        @csrf

        <div class="form-group row">
            <label for="">post title </label>
            <input type="text" name="post_title" value="{{old('post_title')}}">

            @error('post_title')
            <span class="text-danger"> {{$message}}</span>
            @enderror
        </div>

        <div class="form-group row">
            <label for="">conent</label>
            <textarea cols="40" rows="10" type="text" name="post_body">
                {{old('post_title')}}
            </textarea>
            @error('post_body')
            <span class="text-danger"> {{$message}}</span>
            @enderror
        </div>
        <div class="form-group row">
            <label for="">Attachment</label>
            <input type="file" name="post_file">
        </div>


        <div>
            <button type="submit">Submit</button>
        </div>

    </form>


@endsection
