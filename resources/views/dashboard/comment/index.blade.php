@extends('layouts.backend')

@section('content')

    <div class="row">
        <div class="col-12">
            <h4>List of : Comments</h4>
        </div>
        <div class="col-12">
            <table class="table-striped table">

                <tr>
                    <td>Comment</td>
                    <td>Post</td>
                    <td>Content</td>
                    <td>Status</td>
                    <td>Created</td>
                    <td>Actions</td>
                </tr>


                @if(isset($comments) && count($comments)>0)
                    @foreach($comments as $comment)
                        <tr>
                            <td> {{$comment->comment_msg}}</td>
                            <td> {{$comment->post->post_title}}</td>
                            <td> <p>{{Str::limit($comment->post->post_body,100)}}</p></td>
                            <td> {{$comment->post->status}}</td>
                            <td> {{$comment->created_at}}</td>
                            <td>
                                <button>Del</button> </td>
                        </tr>


                    @endforeach
                 @else
                    <tr>
                        <td colspan="6"> No record found.</td>
                    </tr>

                @endif

            </table>
        </div>
    </div>




@endsection
