@extends('layouts.backend')

@section('content')


        @if(isset($posts) && count($posts) > 0 )
            @foreach($posts as $post)
                <div class="row">
                    <div class="col-12">
                        <a  href="#"> <h4>{{$post->post_title}}</h4></a>
                        <span class="font-italic font-weight-bold" for="">Author : {{$post->user->name}}</span>
                        <p>{{$post->post_body}}</p>
                    <div>

                    <div class="row">
                        <div class="col-6">
                            <form method="post" action="{{route('comment.store')}}">
                                @csrf
                                <input type="hidden" name="post_id" value="{{$post->id}}">
                                <textarea name="comment" id="comment" cols="30" rows="2"></textarea>
                                <button type="submit">Comment</button>
                            </form>
                            <span>{{count($post->comment)}} {{ Str::plural('comment', count($post->comment)) }}</span>
                        </div>

                        <div class="col-6">
                            <label class="font-weight-bold" for="">Comments:</label>
                           @foreach($post->comment as $cmt)
                               <p>{{$cmt->comment_msg}}</p>
                               <p>author: {{ \App\User::find($cmt->user_id)->name }}</p>
                                <hr>
                            @endforeach

                        </div>
                    </div>




                    <hr>
                </div>


            @endforeach

        @endif









@endsection
